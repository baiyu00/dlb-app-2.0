# Reading Box 2.0

> 这是 Reading Box 2.0 APP web版

## Build Setup

```bash
# Clone project
git clone https://gitee.com/baiyu00/dlb-app-2.0.git

# Install dependencies
npm install

# Serve with hot reload at localhost:9528
npm run dev

# Build for production with minification
npm run build
