import { GetWallet } from '@/api/api'

export default {
    namespaced: true,
    state: {
        walletInfo: {},  // 钱包信息
    },
    getters: {
        walletInfo: state => {
            return state.walletInfo
		}
    },
    mutations: {
        set_walletInfo(state, val) {
            state.walletInfo = val
		}
    },
    actions: {
        // 获取钱包信息
        async get_walletInfo_data({ commit }) {
            let {data} = await GetWallet()
            commit('set_walletInfo', data)
            return data
		}
    }
}