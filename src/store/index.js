import Vue from 'vue'
import Vuex from 'vuex'
import app_module from './app/index'
import user_module from './user/index'
import index_module from './index/index'
import wallet_module from './wallet/index'
import video_module from './video/index'
Vue.use(Vuex)


const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
}

export default new Vuex.Store({
	state,
    getters,
    mutations,
    actions,
	modules: {
		app: app_module,
		user: user_module,
		index: index_module,
		wallet: wallet_module,
		video: video_module
	}
})