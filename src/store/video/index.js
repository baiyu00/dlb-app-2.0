import cache from '@/utils/cache'
import { GetVideoList } from '@/api/api'

export default {
    namespaced: true,
	state: {
        videoLocation: '',    // 视频栏目的location
		videoList: '',                     // 视频栏目的缓存数据，为了刷新时不用再次请求
	},
	getters: {
        videoLocation: state => {
            return state.videoLocation
		}
    },
	mutations: {
        set_videoLocation(state, val) {
            state.videoLocation = val
            cache.setSession('videoLocation', val)
        },
        set_videoList(state, val) {
            state.videoList = val
            cache.setSession('videoList', val)
        }
	},
	actions: {

        // 获取视频位置缓存
        get_videoLocation_cache({ commit }) {
            const data = cache.getSession('videoLocation')
            if (data) {
                commit('set_videoLocation', data)
            } else {
                commit('set_videoLocation', '0')
            }
            return data
        },

        // 获取视频列表缓存
        get_videoList_cache({ state }) {
            let res = JSON.parse(cache.getSession('videoList'))
            return res
        },

        // 获取视频列表数据
        async get_videoList_data({ commit }, params) {
            let paramsIn = {
                'currentPages': params.page,
                'pageSize': 5,
                'videoId': params.videoId
            }
            let {data} = await GetVideoList(paramsIn)
            return data
        }
  	}
} 