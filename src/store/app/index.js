import cache from '@/utils/cache'
import { getSystemType, isIphonex } from '@/utils'
import { GetAppVersion } from '@/api/api'

export default {
    namespaced: true,
    state: {
		version: '',   // 版本号
        system: '',    // 系统 ios, andriod
        iphonex: '0',     // 是否是Iphonex
        isShowLoginModal: false,  //是否显示登录模块
    },
    getters: {
		version: state => {
            return state.version
		},
		system: state => {
            return state.version
        },
        iphonex: state => {
            return state.iphonex
        },
        isShowLoginModal: state => {
            return state.isShowLoginModal
        }
    },
    mutations: {
		set_version(state, val) {
            state.version = val
			cache.setLocal('version', val)
		},
		set_system(state, val) {
            state.system = val
			cache.setLocal('system', val)
        },
        set_iphonex(state, val) {
            state.iphonex = val
			cache.setLocal('iphonex', val)
        },
        set_isShowLoginModal(state, val) {
            state.isShowLoginModal = val
			cache.setLocal('isShowLoginModal', val)
        }
    },
    actions: {
		// 获取系统类型
		get_system_type({ commit }) {
			if( getSystemType().type == 'android' ){
				commit('set_system', '1')
				return '1'
			}
			if( getSystemType().type == 'ios' ){
				commit('set_system', '2')
				return '2'
			}
		},

		// 获取版本号
        async get_version_data({ commit, state, dispatch }) {
            // 版本号的缓存？缓存 : 重新匹配
			if (cache.getLocal('version')) {
            	return cache.getLocal('version')
			} else {
				dispatch('get_system_type')
                let params = {
                    'systemType': state.system
                }
                let res = await GetAppVersion(params)
                commit('set_version', res.data.versionName)
                return res.data.versionName
            }
        },
        
        // 获取是否为iphonex
		get_iphonex_flag({ commit }) {
            // 取缓存？缓存 : 重新匹配
			if (cache.getLocal('iphonex')) {
            	return cache.getLocal('iphonex')
			} else {
                if( isIphonex() == true ){
                    commit('set_iphonex','1')
                    return '1'
                }else {
                    commit('set_iphonex','0')
                    return '0'
                }
            }
        },
        
        // 获取是否显示登录模块
		get_isShowLoginModal({ commit }) {
            // 取缓存？缓存 : 重新匹配
			if (cache.getLocal('isShowLoginModal')) {
            	return cache.getLocal('isShowLoginModal')
			} else {
                commit('isShowLoginModal',false)
                return false
            }
		}
    }
}