import request from '@/utils/request'

// 登录
export function Login(params) {
  return request({
    url: '/user/login',
    method: 'post',
    data:params
  })
}

// 找回登录密码
export function UpdateLoginPassword(params) {
  return request({
    url: '/user/updateLoginPassword',
    method: 'post',
    data:params
  })
}

// 注册
export function Register(params) {
  return request({
    url: '/user/register',
    method: 'post',
    data: params
  })
}

// 获取验证码
export function GetVerificationCode(params) {
  return request({
    url: '/sms/getVerificationCode',
    method: 'post',
    data: params
  })
}

//刷新token
export function RefreshToken() {
  return request({
    url: '/user/refreshToken',
    method: 'post'
  })
}


//获取安卓下载包
export function GetApk() {
  return request({
    url: '/sys/appVersion/1',
    method: 'get'
  })
}