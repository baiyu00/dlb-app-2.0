import request from '@/utils/request'

// 获取用户信息
export function GetInfo() {
    return request({
      url: '/user/getUser',
      method: 'post',
    })
}

// 获取所有分类列表
export function getOrderList() {
    return request({
        url: '/new/class',
        method: 'get'
    })
}

// 获取已订阅的列表
export function getSubClass() {
    return request({
        url: '/new/userSubClass',
        method: 'get'
    })
}

// 订阅和取消订阅
export function orderClass(params) {
    return request({
        url: '/new/addClassSub',
        method: 'post',
        data: params
    })
}

// 获取新闻分类
export function getNewsOrderList() {
    return request({
        url: '/new/class',
        method: 'get'
    })
}

// 获取新闻列表
export function getNewsList(params) {
    return request({
        url: '/new/list',
        method: 'post',
        data: params
    })
}

// 获取新闻详情
export function getNewsInfo(params) {
    return request({
        url: '/new/info/'+ params.newId,
        method: 'get'
    })
}

// 上传新闻阅读的时间
export function postNewsRead(params) {
    return request({
        url: '/new/read',
        method: 'post',
        data: params
    })
}

// 领取dlb
export function getDlb(params) {
    return request({
        url: '/new/getdlb',
        method: 'post',
        data:params
    })
}

// 修改用户信息
export function UpdateInfo(params) {
    return request({
        url: '/user/update',
        method: 'post',
        data:params
    })
}

// 修改用户信息--头像
export function UpdateInfoByHeadImg(params) {
    return request({
        url: '/user/photo',
        method: 'post',
        data:params
    })
}

// 上传反馈信息
export function PostUserFeedback(params) {
    let formData = new FormData;
    formData.append('file', params.file);
    formData.append("contactMobile", params.contactMobile);
    formData.append("content", params.content);
    return request({
        url: '/user/userFeedback',
        method: 'post',
        data:formData
    })
}

// 获取钱包信息
export function GetWallet() {
    return request({
        url: '/user/wallet',
        method: 'post'
    })
}

// 解锁or锁定
export function PostUnlock(params) {
    return request({
        url: '/wallet/unlock',
        method: 'post',
        data:params
    })
}

// 取消解锁
export function PostCancelLock(params) {
    return request({
        url: '/wallet/cancelLock',
        method: 'post',
        data:params
    })
}

// 解锁列表or锁定列表
export function GetUnlockList(params) {
    return request({
        url: '/wallet/unlockList',
        method: 'post',
        data:params
    })
}

// 验证钱包地址真实存在
export function ValidateAddress(params) {
    return request({
        url: '/wallet/chekoutExtractAddress/'+ params.address,
        method: 'get'
    })
}

// 提现
export function PostExtract(params) {
    return request({
        url: '/wallet/extract',
        method: 'post',
        data:params
    })
}

// 找回支付密码
export function UpdatePayPassword(params) {
    return request({
        url: '/user/updatePayPassword',
        method: 'post',
        data:params
    })
}

// 提现列表
export function GetCashList(params) {
    return request({
        url: '/wallet/extractList',
        method: 'post',
        data:params
    })
}

// 充值列表
export function GetRechargeList(params) {
    return request({
        url: '/wallet/topupRecord/'+ params.size + '/' + params.page,
        method: 'get'
    })
}

// 账单详情
export function GetBillList(params) {
    return request({
        url: '/wallet/billDetails/'+ params.page + '/' + params.size,
        method: 'get'
    })
}

// 获取app版本
export function GetAppVersion(params) {
    return request({
        url: '/sys/appVersion/'+ params.systemType,
        method: 'get'
    })
}

// 实名认证
export function UpdateUserRealInfo(params) {
    let formData = new FormData;
    formData.append('file', params.file[0]);
    formData.append('file', params.file[1]);
    formData.append('file', params.file[2]);
    formData.append("name", params.name);
    formData.append("idNumber", params.idNumber);
    return request({
        url: '/user/updateUserRealInfo',
        method: 'post',
        data:formData
    })
}

// 用户邀请记录
export function GetInvitationList() {
    return request({
        url: '/user/invitationList',
        method: 'get'
    })
}

// 消息列表
export function GetUserMessageList(params) {
    return request({
        url: '/user/userMessageList/'+ params.type +'/'+ params.page + '/' + params.size,
        method: 'get'
    })
}

// 更新消息列表
export function UpdateUserMessageStatus(params) {
    return request({
        url: '/user/userMessageUpdate',
        method: 'post',
        data: params
    })
}

// 用户等级列表
export function GetGradeList() {
    return request({
        url: '/user/gradeList',
        method: 'get'
    })
}

// 购买等级
export function BuyGrade(params) {
    return request({
        url: '/user/buyGrade',
        method: 'post',
        data: params
    })
}

// 获取视频列表
export function GetVideoList(params) {
    return request({
        url: '/video/list',
        method: 'post',
        data: params
    })
}

// 提交视频播放时间数据
export function PostVideoTime(params) {
    return request({
        url: '/video/play',
        method: 'post',
        data: params
    })
}