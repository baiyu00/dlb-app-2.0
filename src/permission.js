import router from './router'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css'// Progress 进度条样式
import { GetAppVersion } from '@/api/api'
import cache from '@/utils/cache'

router.beforeEach((to, from, next) => {
  NProgress.start()
  //判断是否登陆
  var isLogin = cache.getLocal('token');
  //白名单
  var whiteList = ['login','register','findpwd','file','home','video','account','detail'];
  
  if( isLogin ){
    //拉取静态资源是否需要更新
    GetAppVersion({systemType:'3'}).then(res => {
      if(String(res.data.versionNumber) != cache.getLocal('webversion')){
        //重新请求当前页面
        if(cache.getLocal('webversion')){
          window.location.reload();
        }
        cache.setLocal('webversion',res.data.versionNumber,100)
      }
    })
    next()
  }else{
    // 未登录
    if( whiteList.indexOf(to.name) != -1 ){
      //在白名单内--无需强制登录
      next()
    }else{
      //不在白名单内--进入首页
      next({path:'/home'})
    }
  }
  NProgress.done()
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
