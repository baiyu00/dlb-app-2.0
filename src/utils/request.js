import axios from 'axios'
import router from '../router'
import { Toast } from 'mint-ui'
import { RefreshToken } from '@/api/login'
import cache from '@/utils/cache'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.API_ROOT, // api 的 base_url
  timeout: 5000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(
  config => {
    if (cache.getLocal('token')) {
      config.headers['Access-Token'] = 'dlb '+ cache.getLocal('token') // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  response => {
    /**
     * code为非200是抛错 可结合自己业务进行修改
     */
    const res = response.data

    switch (res.code) {
      case 0:    // 错误
        return response.data
      case 200:
        return response.data
      case 4001: //token不能为空
        // Toast('登录信息已失效');
        cache.removeSession()
        cache.removeLocal()
        return response.data
        // router.push({ path: '/login' })
        // break;
      case 4002: //token不合法
        // Toast('登录信息已失效');
        cache.removeSession()
        cache.removeLocal()
        return response.data
        // router.push({ path: '/login' })
        // break;
      case 4003: //token不存在  多点登陆
        Toast('检测您的账号已在另一台设备上登录');
        cache.removeSession()
        cache.removeLocal()
        return response.data
      case 4004: //4004:token已过期 刷新token
        RefreshToken().then(res => {
          if( res.code == 200 ){
            //重置tooken
            cache.setLocal('token', res.data.token)
          }
          //重新请求当前页面
          window.location.reload()
        })
        break;
      case 4005: //4005:禁止3天登录
        Toast('系统检测您使用异常，禁止3天登录');
        cache.removeSession()
        cache.removeLocal()
        return response.data
      case 4006: //4006:账号被冻结
        Toast('系统检测您使用异常，账号已被冻结');
        cache.removeSession()
        cache.removeLocal()
        return response.data
      default:
        return response.data
    }
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
