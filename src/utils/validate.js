
/* 手机号 */
export function validatePhone(str) {
    // const reg = /^1[34578]\d{9}$/
    const reg = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
    return reg.test(str.trim())
}

/* 正整数 */
export function validateInteger(str) {
    const reg = /^(0+)|[^\d]+/
    return reg.test(str.trim())
}

/* 钱包地址 */
// 以0x开头 42位（数字，大小写字母）
export function validateAddress(str) {
    const reg = /^0x[a-zA-Z0-9]{40}$/
    return reg.test(str.trim())
}

/* 身份证号 */
// 15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
export function validateIDcard(str) {
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    return reg.test(str.trim())
}

// 设置登录密码 8-20位，字母、数字、符号的任意两者组合
export function validateLoginpwd(str) {
    const regStr1=/^(\\d)+$/; //数字
    const regStr2=/^[a-zA-z]+$/;//字母
    const regStr3=/^([!@#$%^&*]+)$/;//字符
    const regStr4=/^([A-Z]|[a-z]|[0-9]|[!@#$%^&*]){8,20}$/;

    const regFlag1 = regStr1.test(str.trim());
    const regFlag2 = regStr2.test(str.trim());
    const regFlag3 = regStr3.test(str.trim());
    const regFlag4 = regStr4.test(str.trim());

    if(!regFlag1&&!regFlag2&&!regFlag3){ //不能全是数字，字母，字符
        if(regFlag4){ //只要是数字，字母，字符，8-20位即可
            return true;
        }else{
            return false;
        }
    }else{
        return false
    }
}


