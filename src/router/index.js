import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

// 找回登录密码
const FindPwd = () => import('@/views/FindPwd/index')
// 首页
const Home = () => import('@/views/Home/index')
// 首页-详情页
const Detail = () => import('@/views/Home/children/detail')
// 首页-栏目页
const Channel = () => import('@/views/Home/children/channel')
// 视频
const Video = () => import('@/views/Video/index')
// 我的
const Account = () => import('@/views/Account/Account')
// 我的--钱包
const Wallet = () => import('@/views/Account/Wallet')
// 我的--钱包--账单详情
const Bill = () => import('@/views/Account/Wallet/Bill')
// 我的--钱包--可用
const Usable = () => import('@/views/Account/Wallet/Usable')
// 我的--钱包--可用--记录
const UsableList = () => import('@/views/Account/Wallet/Usable/list')
// 我的--钱包--解锁中
const Unlock = () => import('@/views/Account/Wallet/Unlock')
//  我的--钱包--解锁中--解锁列表
const UnlockList = () => import('@/views/Account/Wallet/Unlock/list')
// 我的--钱包--已锁定
const Lock = () => import('@/views/Account/Wallet/Lock')
// 我的--钱包--提现
const Tocash = () => import('@/views/Account/Wallet/Tocash/tocash')
// 我的--钱包--提现--输入密码
const Inputpwd = () => import('@/views/Account/Wallet/Tocash/inputpwd')
// 我的--钱包--提现--提现结果
const Cashres = () => import('@/views/Account/Wallet/Tocash/Cashres')
// 我的--钱包--充值
const Recharge = () => import('@/views/Account/Wallet/Recharge/index')
// 我的--钱包--充值--充值提现记录
const Record = () => import('@/views/Account/Wallet/Record')
// 我的--邀请好友
const Invite = () => import('@/views/Account/Invite')
// 我的--设置
const Setting = () => import('@/views/Account/Setting')
// 我的--设置--登入密码
const EditLoginPwd = () => import('@/views/Account/Setting/editLoginPwd')
// 我的--设置--支付密码
const EditPayPwd = () => import('@/views/Account/Setting/editPayPwd')
// 我的--设置--支付密码-找回支付密码
const FindPayPwd = () => import('@/views/Account/Setting/findPayPwd')
// 我的--设置--修改手机号
const EditPhone = () => import('@/views/Account/Setting/editPhone')
// 我的--设置--关于我们
const About = () => import('@/views/Account/Setting/about')
// 我的--设置--帮助中心
const Help = () => import('@/views/Account/Setting/help')
// 我的--设置--意见反馈
const Feedback = () => import('@/views/Account/Setting/feedback')
// 我的--消息中心
const MyMessage = () => import('@/views/Account/Message')
// 我的--消息中心--详情
const MyMessageDetail = () => import('@/views/Account/Message/detail')
// 我的--等级中心
const MyLevel = () => import('@/views/Account/Level')
// 我的--购买中心
const Purchase = () => import('@/views/Account/Purchase')
// 我的--编辑
const MyInfo = () => import('@/views/Account/MyInfo')
// 我的--编辑--修改头像
const EditHeadImg = () => import('@/views/Account/MyInfo/editHeadImg')
// 我的--编辑--修改昵称
const EditNickname = () => import('@/views/Account/MyInfo/editNickname')
// 我的--编辑--修改个人简介
const EditIntro = () => import('@/views/Account/MyInfo/editIntro')
// 我的--实名认证
const RealName = () => import('@/views/Account/RealName')

export const constantRouterMap = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/findpwd',
    name: 'findpwd',
    meta: {
      keepAlive: true
    },
    component:FindPwd
  },
  {
    path: '/home',
    name: 'home',
    meta: {
      keepAlive: true
    },
    component:Home
  },
  {
    path: '/channel',
    name:'channel',
    meta: {
      keepAlive: true
    },
    component: Channel
  },
  {
    path: '/detail',
    name:'detail',
    meta: {
      keepAlive: false
    },
    component: Detail
  },
  {
    path: '/video',
    name:'video',
    meta: {
      keepAlive: true
    },
    component: Video
  },
  {
    path: '/account',
    name: 'account',
    meta: {
      keepAlive: true
    },
    component: Account
  },
  {
    path:'/account/wallet',
    name: 'wallet',
    meta: {
      keepAlive: true
    },
    component: Wallet
  },
  {
    path:'/account/wallet/bill',
    name: 'bill',
    meta: {
      keepAlive: false
    },
    component: Bill
  },
  {
    path:'/account/wallet/usable',
    name: 'usable',
    meta: {
      keepAlive: false
    },
    component: Usable
  },
  {
    path:'/account/wallet/usableList',
    name: 'usableList',
    meta: {
      keepAlive: false
    },
    component: UsableList
  },
  {
    path:'/account/wallet/tocash',
    name: 'tocash',
    meta: {
      keepAlive: false
    },
    component: Tocash
  },
  {
    path:'/account/wallet/inputpwd',
    name: 'inputpwd',
    meta: {
      keepAlive: false
    },
    component: Inputpwd
  },
  {
    path:'/account/wallet/cashres',
    name: 'cashres',
    meta: {
      keepAlive: true
    },
    component: Cashres
  },
  {
    path:'/account/wallet/recharge',
    name: 'recharge',
    meta: {
      keepAlive: true
    },
    component: Recharge
  },
  {
    path:'/account/wallet/unlock',
    name: 'unlock',
    meta: {
      keepAlive: false
    },
    component: Unlock
  },
  {
    path:'/account/wallet/unlocklist',
    name: 'unlocklist',
    meta: {
      keepAlive: false
    },
    component: UnlockList
  },
  {
    path:'/account/wallet/lock',
    name: 'lock',
    meta: {
      keepAlive: false
    },
    component: Lock
  },
  {
    path:'/account/wallet/record',
    name: 'record',
    meta: {
      keepAlive: false
    },
    component: Record
  },
  {
    path:'/account/invite',
    name: 'invite',
    meta: {
      keepAlive: true
    },
    component: Invite
  },
  {
    path:'/account/setting',
    name: 'setting',
    meta: {
      keepAlive: true
    },
    component: Setting
  },
  {
    path:'/account/setting/editLoginPwd',
    name: 'editLoginPwd',
    meta: {
      keepAlive: false
    },
    component: EditLoginPwd
  },
  {
    path:'/account/setting/editPayPwd',
    name: 'editPayPwd',
    meta: {
      keepAlive: false
    },
    component: EditPayPwd
  },
  {
    path:'/account/setting/editPayPwd/findPayPwd',
    name: 'findPayPwd',
    meta: {
      keepAlive: false
    },
    component: FindPayPwd
  },
  {
    path:'/account/setting/editPhone',
    name: 'editPhone',
    meta: {
      keepAlive: false
    },
    component:EditPhone
  },
  {
    path:'/account/setting/about',
    name: 'about',
    meta: {
      keepAlive: true
    },
    component: About
  },
  {
    path:'/account/setting/help',
    name: 'help',
    meta: {
      keepAlive: true
    },
    component: Help
  },
  {
    path:'/account/setting/feedback',
    name: 'feedback',
    meta: {
      keepAlive: false
    },
    component: Feedback
  },
  {
    path:'/account/message',
    name: 'myMessage',
    meta: {
      keepAlive: false
    },
    component: MyMessage
  },
  {
    path:'/account/message/detail',
    name: 'myMessageDetail',
    meta: {
      keepAlive: false
    },
    component: MyMessageDetail
  },
  {
    path:'/account/level',
    name: 'myLevel',
    meta: {
      keepAlive: true
    },
    component: MyLevel
  },
  {
    path:'/account/purchase',
    name: 'purchase',
    meta: {
      keepAlive: true
    },
    component: Purchase
  },
  {
    path:'/account/myInfo',
    name: 'myInfo',
    meta: {
      keepAlive: true
    },
    component: MyInfo
  },
  {
    path:'/account/myInfo/editHeadImg',
    name: 'editHeadImg',
    meta: {
      keepAlive: false
    },
    component: EditHeadImg
  },
  {
    path:'/account/myInfo/editNickname',
    name: 'editNickname',
    meta: {
      keepAlive: false
    },
    component: EditNickname
  },
  {
    path:'/account/myInfo/editIntro',
    name: 'editIntro',
    meta: {
      keepAlive: false
    },
    component: EditIntro
  },
  {
    path:'/account/realName',
    name: 'RealName',
    meta: {
      keepAlive: false
    },
    component: RealName
  }
]

export default new Router({
  // mode: 'history', //后端支持可开
  routes: constantRouterMap
})
